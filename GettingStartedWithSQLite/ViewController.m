//
//  ViewController.m
//  GettingStartedWithSQLite
//
//  Created by James Dreger on 3/10/14.
//  Copyright (c) 2014 James Dreger. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // Get the documents directory
    NSString *docsDir;
    NSArray *dirPaths;
    dirPaths = NSSearchPathForDirectoriesInDomains(
                                                   NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    
    // Build the path to the database file
    _databasePath = [[NSString alloc]
                     initWithString: [docsDir stringByAppendingPathComponent:
                                      @"users.db"]];
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    // Check if database file exists
    if ([filemgr fileExistsAtPath: _databasePath ] == NO)
    {
        const char *dbpath = [_databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &_usersDB) == SQLITE_OK)
        {
            char *errMsg;
            const char *sql_stmt =
                "CREATE TABLE IF NOT EXISTS users (userId INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT, password TEXT, access TEXT)";
            
            // Create database
            if (sqlite3_exec(_usersDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                _status.text = @"Failed to create table";
                NSLog(@"Failed to create table");
            }
            else
            {
                NSLog(@"Created table");
            }
            sqlite3_close(_usersDB);
        } else {
            _status.text = @"Failed to open/create database";
            NSLog(@"Failed to open/create database");
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)save:(id)sender {
    // Create sqlite statement
    const char *dbpath = [_databasePath UTF8String];
    sqlite3_stmt    *statement;
    
    // Open database
    if (sqlite3_open(dbpath, &_usersDB) == SQLITE_OK)
    {
        // SQL
        NSString *insertSQL = [NSString stringWithFormat:
                               @"INSERT INTO users (username, password, access) VALUES (\"%@\", \"%@\", \"%@\")",
                               self.username.text, self.password.text, self.access.text];
        
        const char *insert_stmt = [insertSQL UTF8String];
        // Prepare SQL statement
        sqlite3_prepare_v2(_usersDB, insert_stmt,
                           -1, &statement, NULL);
        // Execute SQL statement
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            self.status.text = @"User added";
            self.username.text = @"";
            self.password.text = @"";
            self.access.text = @"";
        } else {
            self.status.text = @"Failed to add contact";
            NSLog(@"Failed to add contact");
        }
        sqlite3_finalize(statement);
        sqlite3_close(_usersDB);
    }
}


- (IBAction)checkUser:(id)sender {
    // Create sqlite statement
    const char *dbpath = [_databasePath UTF8String];
    sqlite3_stmt    *statement;
    
    // Open database
    if (sqlite3_open(dbpath, &_usersDB) == SQLITE_OK)
    {
        // SQL
        NSString *querySQL = [NSString stringWithFormat:
                              @"SELECT username, access FROM users WHERE username=\"%@\" AND password=\"%@\" ",
                              self.username.text, self.password.text];
        
        const char *query_stmt = [querySQL UTF8String];
        // Prepare SQL statement
        if (sqlite3_prepare_v2(_usersDB,
                               query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            // Execute SQL statement
            // if you expect mutliple results use while instead of if
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                // Get First Column from SQL
                NSString *userName = [[NSString alloc]
                                          initWithUTF8String:(const char *)
                                          sqlite3_column_text(statement, 0)];
                // Get Second Column from SQL
                NSString *accessField = [[NSString alloc]
                                        initWithUTF8String:(const char *)
                                        sqlite3_column_text(statement, 1)];
                self.status.text = [NSString stringWithFormat:@"UserName: %@ Access:%@",userName,accessField];
            } else {
                self.status.text = @"Match not found";
                self.password.text = @"";
                self.access.text = @"";
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(_usersDB);
    }
}

@end
